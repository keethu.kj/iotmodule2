# Board Electrical Safety instructions  
## Power Supply
Impt points:  
1. output voltage match input voltage
2. connect properly and check power rating before turning on
3. Do not connect **higher output** to **lower input**  

## Handling
Impt points:
1. ensure plugged in devices are handled with care
2. use dry hands and stable surface
3. keep contact points closed and use usb fan to cool 

## GPIO
Impt points:  
1. Identify if 3.3 V or 5 V logic and use logic level converter to connect peripherals respectively adding the appropriate sensors.
2. Do not go over 5 V for 3.3 V pin and connect any sensors with an appropriate resistor.
3. Always turn off before making connections and ensure connections are within voltage range.  
4. Use Transistor while connecting motor, this way you can control loads with higher electrical requirements.  

## Guidelines for Interfaces  

1. **UART**  
   - Rx pin of first device connected to Tx pin of second device and vice versa.
   - Use voltage divider if working voltage levels vary  
   - Without protection circuit, UART facilitates communication from usb to TTL  
   - sensor interfacing requires protection circuit  
2. **I2C**  
   - With sensors, SDL and SDA **MUST** be protected by using in-built pull up registers  
   - If connecting on breadboard use pull up resistors ranging from 2.2KOhm to 4KOhm  
3. **SPI**  
   - Since SPI is in push pull mode by default, does not require protection circuit  
   - using >1 slave, seond device can **'hear'** and **'respond'** even with device 1 as disturbance, hence protection circuit with pull up used on each slaveselect  
   - 1kOhm<resistor value<10kOhm or ~4.7kOhm
   
**Comparison of Interfaces**  

| UART | I2C | SPI |
| ------ | ------ | ------ |
| Well Known, Cost effective and Simple | Simple, Well Known, Universally accepted, Plug and play, Large portfolio and Cost effective | Fast, Universally accepted, Low cost and Large portfolio |
| Limited functionality, Point to Point | Limited Speed | No Plug and Play HW, No "fixed" standard |






# IIOT PROTOCOLS  
____________________________________________________________________________

## 4-20mA
Most dominant in the process control industry  due to reasons similar to 3-15psi standard  for air compression. 
   - Under 3 psi, expensive to engineer and unrecognizable
   - difficult to identify failure in system(0 psi)  

Works by following Ohm's Law, **V=IxR** 
![Ohm's Law](https://www.grc.nasa.gov/www/k-12/airplane/Images/ohms.jpg)  

Every element in circuit either **provides voltage** or has **voltage drop** however, current is the same throughout the loop.  

Components of 4-20mA  
![Components](https://www.azom.com/images/Article_Images/ImageForArticle_15057(1).jpg)  

1. **Sensor**  
   Technology of sensor varies based on specific measured process variable  
2. **Transmitter**  
   Sensor input converted to current signal btwn 4-20 mA  
3. **Power Source**  
   Should output DC current to produce signal.
   Power supply should be atleast 10%> than total voltage drop of attached components or else could lead to circuit failure
4. **Loop**  
   From actual wire from sensor to device receiving signal back to transmitter, loop exists. Wire adss resistance if present over long distances  
5. **Receiver**  
   Received signal translated into units understood by operators and info is either displayed or automatically does something like controllers/actuators  

**Pros and cons**  

| Pros | Cons |
| ------ | ------ |
| dominant in many industries | current loops can only process one signal |
| simplest option to connect  | numerous process requires multiple loops  |
| less wiring->reduce initial setup costs | could lead to ground loops if independent loops not isolated properly |
| current does not degrade along long distances |  number of loops increase->isolation requirement exponentially more complicated |
| less sensitive to electrical noise | |
| very simple to detect fault in system| |
________________________________________________________________________________

## Modbus  

**What is Modbus**  
Communication protocol for use with PLCs. 
Transmits signals/info from Intrumentation and control devices to main controller/data gathering system. 
Master(1)-device requesting info  
Slave(upto 247)-device supplying info
Communication happens through function code which specifies action to be performed  
![working](https://www.rfwireless-world.com/images/Modbus-protocol.jpg)

Used over two interfaces:
- RS485->Modbus RTU  
- Ethernet->Modbs TCP/IP

**RS485**  
![What is it](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR8dMy2BIPd0g-WmHmNI5pejtH3G6J5iR2kBw&usqp=CAU)
![Specifications](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREJC6rm8E8jrEqVYKO6TcYgBJP9LaAeo5PBsqBq5xLESEE7qZF&s)

________________________________________________________________________________

## OPCUA  
Open Standard that specifies information exchange for industrial Communication
It is highly scalable  
can be used in closed network or via internet  

**Fundamentals**  
- Client/server based  
- client requests->server responds  
- OPCUA regulates data fetched  
- doubles as Pub-Sub system  

**Advantages**  
- High Scalability, Performance and security and access control  
- Internet and firewall  
- Object-Oriented  
- Abstract Base Model  
- Platform Independent  

**Unified Architecture(UA)**  
- Data structures used to manage all data info and sends to cloud all at once  
- Supports OOPs  
- Centers Transport Mechanism and Data Modelling  
- Access smallest part of complex data mesh  

**Security Model**  
- Asymmetric encryption-> Key agreement
- Symmetric encryption->securing comm.  
- Authentication of clients and protects Confidentiality  

**Protocols**  
- self-reliant and independent  
- Data Access(DA): fetches data from control system  
- Alarm & Events(AE): Subscription based and no storage system  
- Historical Analysis(HDA): historical data through SCADA  

![OPCUA](https://img.automationworld.com/files/base/pmmi/all/image/2011/09/aw_4805_burkeopcua.png?auto=format&w=500)

![Server](https://ars.els-cdn.com/content/image/1-s2.0-S0920548913000640-gr2.jpg)
_______________________________________________________________________________________________________________________________________________________

# MQTT(MQ Telemetry Transport) Protocol  

- lightweight messaging protocol  
- pub-sub system  
- for devices with varying latencies due to low bandwidth  
- reads and publishes data from sensor nodes  
- sends required commands to control outputs  
- simultaneous communication between several devices can be established  

![MQTT protocol](https://www.rfwireless-world.com/images/Broker-based-MQTT-protocol.jpg)

MQTT Client(Publisher)--sends message-->MQTT broker--distribute message-->other subscribed MQTT clients  
Topics--> register interest for incoming messages/specify where to publish-->Rep by strings and slashes
_______________________________________________________________________________________________________________________________________________________

# HTTP(Hyper Text Transport Protocol)  

- based on request from client and response from server  
- TCP/IP based communication protocol  
- Default port is TCP 80 

**Basic Architecture:**
![Architecture](https://www.tutorialspoint.com/http/images/cgiarch.gif)

**Basic Features**:
1. Connectionless  
2. Media Independent  
3. Stateless  

**Request has three parts:**  
1. Request line  
2. HTTP headers  
3. message body  
**Example of Request:**  
![Request](https://media.prod.mdn.mozit.cloud/attachments/2016/08/09/13687/5d4c4719f4099d5342a5093bdf4a8843/HTTP_Request.png)

| HTTP Method | CRUD | Description | Example | 
| ------ | ------ | ------ | ------ |
| GET | Read/Retrieve | Getting a Resource | Getting a Recipe with ID=20(http://localhost:5000/recipes/20) |
| POST | Create | Creating a Resource | Adding a Recipe(http://localhost:5000/recipes) |
| PUT | Update | Updating a Resource | Updating a Recipe with ID=20(http://localhost:5000/recipes/20) |
| DELETE | Delete | Deleting a Resource | Deleting a recipe with ID=20(http://localhost:5000/recipes/20) |


   
**Response also has three parts:**  
1. Status Line  
2. HTTP header  
3. Message Body  
**Example of Response:**  
![Response](https://media.prod.mdn.mozit.cloud/attachments/2016/08/09/13691/58390536967466a1a59ba98d06f43433/HTTP_Response.png)

**HTTP Staus Codes:**  

![HTTP Status codes](https://pbs.twimg.com/media/D-bI-xyWkAAY0Qb?format=jpg&name=medium)

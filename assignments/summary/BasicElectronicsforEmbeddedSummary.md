# Basic Electronics for Embedded
_______________________________________________________________________________
## Sensors and Actuators  

**NOT** the same

**Sensors**
Sensors are transducers that converts physical energy into an electrical signal that can be measured.  
 ![Examples of Sensors](https://i0.wp.com/iot4beginners.com/wp-content/uploads/2018/12/different-types-of-sensor.png?w=920&ssl=1)

**Actuators**  
Reverse of Sensors, they take in an electrical input and return physical action
![Examples of Actuators](https://i2.wp.com/robocademy.com/wp-content/uploads/2020/04/dc_motors.png?resize=1024%2C513&ssl=1)
_______________________________________________________________________________
## Analog and Digital  

| Parameters | Analog | Digital |
| ------ | ------ | ------ |
| Signal | continuos signal representing physical measurements | discrete time signals by digital modulation |
| Waves | Sine waves  ![continuous](https://media.test.cheggcdn.com/comis/c6e/c6ec9549-0522-4597-9f6f-3c98972e1dae/CL-9681V1.png) | Square waves ![Discrete](https://d2vlcm61l7u1fs.cloudfront.net/media%2F1b5%2F1b5f9269-0b70-46d7-a1d7-1dbeffaec9d5%2Fphpr94O3i.png) |
| Representation | continuous range | discontinuous values |
| Example | analog electronic devices | Computers, mp3s, cell phones |
| Technology | waveforms recorded as they are ![analog waveforms](https://www.weschler.com/wp-content/uploads/2018/05/K-241-ACV.jpg) | recorded as limited set of numbers ![Digital waveforms](https://5.imimg.com/data5/AQ/KG/MY-582164/double-display-digital-volt-ampere-meter-500x500.jpg) |
| Applications | audio amplifiers, power amplifiers, power converter | radar, sonar, digital synthesizers |
_______________________________________________________________________________
## Microcontrollers vs Microprocessors  

![Differences between microcontrollers and microprocessors](http://cdn.differencebetween.net/wp-content/uploads/2018/02/Microprocessor-VERSUS-Microcontroller-.jpg)

| Parameters| Arduino Uno | Raspberry Pi |
| ------ | ------ | ------ |
| What does it have? | RAM: 2K, Flash: 32K, Timers, Serial(UART), I2C, SPI |32-bit microprocessors, RAM : 512 MB, ports that support Ethernet, USB, Audio, Video, SD Card, HDMI, GPIO |
| Features | No Interpreter, No OS, No Firmware | Run within Linux OS, Has firmware and Basic interpreter |
| Which one is better? | For controlling things  | To process lots of data |
_______________________________________________________________________________
## Intro to RPI

![Raspberry Pi](https://raw.githubusercontent.com/SeeedDocument/Raspberry-Pi-4/master/img/hardware-1024.jpg)  

**What is it?**
- Small Single Board Computers(Microprocessors) 
- SoC: IC that integrates all components of computer  
- Power Supply of 5.1V@2.5A: Large but limited  
- No other storage features  
- add on functionalities from Shield  
- Uses ARM processor and runs on Linux OS(Raspbian software can be downloaded)  
- Sensors and Actuators connected to 17 digital GPIO pins
- can be interface with other Pis  or through GPIO, UART, SPI, I2C, PWM  

*interface: connected to microprocessor  
_______________________________________________________________________________
## Serial and Parallel Communication  

| Serial Communication | Parallel Communication |
| ------ | ------ |
| One bit at a time sent over a Communication channel(**Sequentially**) | Multiple bits sent as whole on link with several parallel channels |
| Single line to send data ![serial](https://circuitglobe.com/wp-content/uploads/2019/07/serial-communication.jpg)| Several lines to send data ![parallel](https://circuitglobe.com/wp-content/uploads/2019/07/parallel-communication.jpg) 
| UART, SPI, I2C | GPIO |

